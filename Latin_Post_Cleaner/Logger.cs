﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Latin_Post_Cleaner
{
    public class Logger
    {

        private readonly static object semaphoreWriteTrace = new object();

        /*
         * TRACE LOG
         */
        public Boolean writeTraceLog(String information)
        {
            //FILE PATH
            String currentDateTime_Log = DateTime.Now.ToString("yyyy-MM-dd");
            String filePath = AppContext.BaseDirectory;
            String file = filePath + "postCleaner_log.log";


            lock (semaphoreWriteTrace) //SEMAPHORE
            {

                //WRITE INTERFACE FILE
                using (StreamWriter writetext = new StreamWriter(file, true))
                {
                   
                    /////
                    String strWrite = "";
                    strWrite = strWrite + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + " -- ";
                    strWrite = strWrite + "Informacion: " + @information;

                    writetext.WriteLine(strWrite);


                    //CLOSE FILE
                    writetext.Close();
                }

            }


            return true;

        }//END METHOD


    }
}
