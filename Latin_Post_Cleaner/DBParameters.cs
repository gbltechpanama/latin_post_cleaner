﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Latin_Post_Cleaner
{
    public class DBParameters
    {
        public string DBUrl { get; set; }
        public string DBUsername { get; set; }
        public string DBPassword { get; set; }
        public String DBName { get; set; }
    }
}
