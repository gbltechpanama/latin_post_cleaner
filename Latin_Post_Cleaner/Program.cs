﻿using System;

namespace Latin_Post_Cleaner
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Programa Iniciado");


            //CLEAN TYPE 1 POST (10 DAYS)
            new PostManager().cleanPost(1);

            //CLEAN TYPE 2 POST (10 DAYS)
            //new PostManager().cleanPost(2);

            //CLEAN TYPE 3 POST (30 DAYS)
            new PostManager().cleanPost(3);

            Console.WriteLine("Programa Finalizado");

        }

    }

}
