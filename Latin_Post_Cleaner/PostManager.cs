﻿#pragma warning disable
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Latin_Post_Cleaner
{
    public class PostManager
    {

        SqlConnection cnn;


        public SqlConnection connect()
        {

            //READ FROM DATABASE CONEXION FILE
            String currentDirectory = AppContext.BaseDirectory;
            String readText = System.IO.File.ReadAllText(@currentDirectory + "DBParameters.json");

            DBParameters objParameters = new DBParameters();

            objParameters = JsonConvert.DeserializeObject<DBParameters>(readText);


            String connectionString =
                "Data Source=" + objParameters.DBUrl + ";" +
                "Persist Security Info = True; " +
                "Connection Timeout = 500; " +
                "Initial Catalog =" + objParameters.DBName + ";" +
                "User Id=" + objParameters.DBUsername + ";" +
                "Password=" + objParameters.DBPassword + ";";

            cnn = new SqlConnection(connectionString);


            //OPEN CONNECTION
            cnn.Open();

            return cnn;
        }



        public Boolean disconnect()
        {
            cnn.Close();

            return true;
        }


        public void cleanPost(Int32 postType)
        {

            //QUERY
            String sQuery = "";
            Int32 rowsAffected = 0;

            switch (postType)
            {

                case 1:

                    //
                    sQuery = "UPDATE POSTS SET ID_STATUS = 3 WHERE DATEDIFF(DAY, GETDATE(),POST_DATE)  > 10 AND ID_STATUS = 2 AND ID_TYPE_PLAN = " + postType ;
                    //

                    this.connect();

                    rowsAffected = new SqlCommand(sQuery, this.cnn).ExecuteNonQuery();

                    this.disconnect();

                    //WRITE LOG
                    new Logger().writeTraceLog("TIPO POST: 1, CANT POST ACTUALIZADOS: " + rowsAffected.ToString());

                    break;


                /*case 2:

                    //
                    sQuery = "UPDATE POSTS SET ID_STATUS = 3 WHERE DATEDIFF(DAY, GETDATE(),POST_DATE)  > 10 AND ID_STATUS = 2 AND ID_TYPE_PLAN = " + postType;
                    //

                    this.connect();

                    rowsAffected = new SqlCommand(sQuery, this.cnn).ExecuteNonQuery();

                    this.disconnect();

                    //WRITE LOG
                    new Logger().writeTraceLog("TIPO POST: 2, CANT POST ACTUALIZADOS: " + rowsAffected.ToString());

                    break;
                */


                case 3:

                    //
                    sQuery = "UPDATE POSTS SET ID_STATUS = 3 WHERE DATEDIFF(DAY, GETDATE(),POST_DATE)  > 30 AND ID_STATUS = 2 AND ID_TYPE_PLAN = " + postType;
                    //

                    this.connect();

                    rowsAffected = new SqlCommand(sQuery, this.cnn).ExecuteNonQuery();

                    this.disconnect();

                    //WRITE LOG
                    new Logger().writeTraceLog("TIPO POST: 3, CANT POST ACTUALIZADOS: " + rowsAffected.ToString());

                    break;

            }//END SWITCH
            
        }//END METHOD

    }//END CLASS

}//END NAMESPACE
